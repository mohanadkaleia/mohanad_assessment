"""Task 2

Revision ID: d327e189eb5
Revises: 00000000
Create Date: 2018-04-25 09:55:23.578221

"""

# revision identifiers, used by Alembic.
revision = 'd327e189eb5'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    # Increase the point_balance for user 1 to 1000
    op.execute('''UPDATE user SET
               point_balance = 1000
               WHERE
               user_id = 1
    ''')

    # Add a location for user 2, assuming they live in the USA
    op.execute('''INSERT INTO rel_user
               (user_id, rel_lookup, attribute)
               VALUES
               (2, 'LOCATION', 'USA')
    ''')

    # Change the tier for user 3 to Silver
    op.execute('''UPDATE user
                  SET
                  tier = 'Silver'
                  WHERE
                  user_id = 3
    ''')


def downgrade():
    pass
