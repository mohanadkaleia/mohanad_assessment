from tests import OhmTestCase
from models import *

class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []


    def test_get_recent_users(self):
        recent_users = User.get_recent_users()
        # Check total number of records should be 3
        assert recent_users.rowcount == 3
        # Check the first user tier should be tier
        assert recent_users.fetchone()['tier'] == 'Silver'
        # Check phone for the second one
        assert recent_users.fetchone()['phones'] == '+14086551234'
        # Check phones
        assert recent_users.fetchone()['phones'] == '+14086441234,+14086445678'
