from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User
import time

def get_recent_users(limit):    
    return User.get_recent_users(limit)

@app.route('/community', methods=['GET'])
@app.cache.memoize(timeout=300)
def community():
    """
    List the 5 most recent users (most recently signed up user first),
    with columns for user's display_name, tier, point_balance, and phone number(s).
    """
    login_user(User.query.get(1))
    users = get_recent_users(5)
    args = {
            'gift_card_eligible': True,
            'cashout_ok': True,
            'user_below_silver': current_user.is_below_tier('Silver'),
            'recent_users': users
    }

    return render_template("community.html", **args)
